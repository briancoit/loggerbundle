<?php

namespace BrianCoit\LoggerBundle\Processor;

/**
 * Provides additional logger caller data
 *
 * @author Brian Coit <me@brian.co.it>
 */
class Processor
{
    /**
     * Process record
     *
     * @param array $record
     * @return array
     */
    public function processRecord(array $record)
    {
        $backtrace = debug_backtrace();
        $matched = false;

        $record['extra']['caller_class'] = null;
        $record['extra']['function'] = null;
        $record['extra']['pid'] = getmypid();
        if($backtrace) {
            foreach($backtrace as $i => $data) {
                    if (isset($data['class']) && $data['class'] == 'Monolog\Logger') {
                        $matched = true;
                    } else if ($matched) {
                        $record['extra']['caller_class'] = isset($data['class']) ? $data['class'] : null;
                        $record['extra']['function'] = isset($data['function']) ? $data['function'] : null;
                        break;
                    }
            }
        }


        return $record;
    }
}
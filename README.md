# LoggerBundle

## Introduction
Provides support for logging calling class, method and PID

### Configuration

`app\config.yml`:
```yaml
monolog:
    handlers:
        main:
            type: stream
            path: "%kernel.logs_dir%/%kernel.environment%.log"
            level: debug
            formatter: briancoit.logger.formatter
```